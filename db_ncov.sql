/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1_3306
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_ncov

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 01/02/2020 19:25:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_area_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_area_data`;
CREATE TABLE `tb_area_data`  (
  `id` int(10) UNSIGNED NOT NULL COMMENT '丁香园方ID',
  `create_time` bigint(10) UNSIGNED NOT NULL COMMENT '创建时间',
  `modify_time` bigint(10) UNSIGNED NOT NULL COMMENT '更新时间',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签',
  `country_type` tinyint(3) UNSIGNED NOT NULL COMMENT '国家类型',
  `province_id` int(10) UNSIGNED NOT NULL COMMENT '省ID',
  `province_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省名',
  `province_short_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省短名',
  `city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市名称',
  `confirmed_count` int(10) UNSIGNED NOT NULL COMMENT '确诊数量',
  `suspected_count` int(10) UNSIGNED NOT NULL COMMENT '疑似数量',
  `cured_count` int(10) UNSIGNED NOT NULL COMMENT '治愈数量',
  `dead_count` int(10) UNSIGNED NOT NULL COMMENT '死亡数量',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注释',
  `sort` int(10) UNSIGNED NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`, `modify_time`) USING BTREE,
  INDEX `country_type`(`country_type`, `province_name`, `modify_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_city_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_city_data`;
CREATE TABLE `tb_city_data`  (
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT '丁香园方省ID',
  `province_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属省名称',
  `city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市名称',
  `confirmed_count` int(10) UNSIGNED NOT NULL COMMENT '确诊数量',
  `suspected_count` int(10) UNSIGNED NOT NULL COMMENT '疑似数量',
  `cured_count` int(10) UNSIGNED NOT NULL COMMENT '治愈数量',
  `dead_count` int(10) UNSIGNED NOT NULL COMMENT '死亡数量',
  `modify_time` bigint(10) UNSIGNED NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`parent_id`, `city_name`, `modify_time`) USING BTREE,
  INDEX `province_name`(`province_name`, `city_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_statistics
-- ----------------------------
DROP TABLE IF EXISTS `tb_statistics`;
CREATE TABLE `tb_statistics`  (
  `id` int(10) UNSIGNED NOT NULL,
  `create_time` bigint(10) UNSIGNED NOT NULL COMMENT '创建时间',
  `modify_time` bigint(10) UNSIGNED NOT NULL COMMENT '更新时间',
  `infect_source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '传染源',
  `pass_way` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '传播途径',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全国分布图',
  `daily_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日走势图',
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '摘要',
  `count_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '统计备注',
  `confirmed_count` int(10) UNSIGNED NOT NULL COMMENT '确诊数量',
  `suspected_count` int(10) UNSIGNED NOT NULL COMMENT '疑似数量',
  `cured_count` int(10) UNSIGNED NOT NULL COMMENT '治愈数量',
  `dead_count` int(10) UNSIGNED NOT NULL COMMENT '死亡数量',
  `virus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '病毒名称',
  `remark1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注1',
  `remark2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注2',
  `remark3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注3',
  `remark4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注4',
  `remark5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注5',
  `general_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '一般说明',
  `abroad_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '国外备注',
  UNIQUE INDEX `modify_time`(`modify_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
